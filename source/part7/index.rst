.. _part7:

*************************************************************************************************
Partie 7 | Bynames implicits
*************************************************************************************************

Question proposed by Group 38, Florian Vranckx and Sophie Schorochoff
=====================================================================
Question 1 Explain the difference between "Call by name" and "Call by value" :
------------




Question 2. Detail all the computational steps of a "Call by name" of the following definition
------------
.. code-block:: scala

	def square(x: Double) = multiply(x,x)
	def cube(x: Double) = multiply(square(x),x)
	def multiply(x:double, y:double) = x*y

	cube(1+1)


Question 3. What is display when we execute the main of the object A
------------
.. code-block:: scala

	object A {
	    var count = 1
	    def functionA(n :Int) (body : () => Unit): Unit ={
	        for(i <- 1 to n){
	            println("iteration " + i + ", count = " + count)
	            body()
	        }
	    }
	    def main(args: Array[String]): Unit = {
	        functionA(5){() => count = count + 1 }
	    }
	}

Question 4. What is display when we execute the main of the object B
------------
.. code-block:: scala

	object B {
	    var count = 1
	    def functionB(n :Int)( body : Unit): Unit ={
	        for(i <- 1 to n){
	            println("iteration " + i + ", count = " + count)
	            body
	        }
	    }
	    def main(args: Array[String]): Unit = {
	        functionB(5){count = count + 1}
	    }
	}

""""""""""""""

Answers
=======

Question 2:
----------------------------------------------------------------------
.. code-block:: scala

	cube(1+1)
	multiply(square(1+1),1+1)
	square(1+1)*(1+1)
	multiply((1+1),(1+1))*(1+1)
	(1+1)*(1+1)*(1+1)
	2*(1+1)*(1+1)
	2*2*(1+1)
	2*2*2
	4*2
	8
	
	
Question 3:
----------------------------------------------------------------------
.. code-block:: scala

	iteration 1, count = 1
	iteration 2, count = 2
	iteration 3, count = 3
	iteration 4, count = 4
	iteration 5, count = 5

Question 4:
----------------------------------------------------------------------
.. code-block:: scala

	iteration 1, count = 2
	iteration 2, count = 2
	iteration 3, count = 2
	iteration 4, count = 2
	iteration 5, count = 2


""""""""""""""

Question proposed by Group 17, Chenoy Antoine and Ponchau Thomas
=====================================================================
Question 1. When we work with streams, it is better to use call by name or call by value ? Why ?
------------

Question 2. What is the use of an implicit class ? Give an example.
------------

""""""""""""""

Answers
=======
Question 1 :
----------------------------------------------------------------------
Call by name. It allows to avoid evaluating the tail until we need it.

Question 2:
---------------------------------------------------------------------
Enrich an existing object. Example :

Let's enrich Int's with the prime testing functionality:
----------------------------------------------------------------------

.. code-block:: scala

	object TestImplicits extends App {
	    implicit class PrimeTester(i: Int) {
	        def isPrime: Boolean = {
	            if (i <= 1) false
	            else if (i == 2) true
	            else !(2 to (i - 1)).exists(x => i % x == 0)
	        }
	    }
	    3.isPrime
	}

""""""""""""
